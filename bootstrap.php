<?php

define('ENTITY_MANAGER','entityManager');

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;


require_once __DIR__.'/vendor/autoload.php';

$dir = __DIR__;
if (file_exists(__DIR__.'/../.env')) {
    $dir = __DIR__.'/..';
}

$dotenv = \Dotenv\Dotenv::create($dir);
$dotenv->load();


// Create a simple "default" Doctrine ORM configuration for Annotations
$isDevMode = true;
$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src",__DIR__."/src/Model"), $isDevMode, null, null, false);

// database configuration parameters
$conn = array(
    'driver' => $_ENV['DB_DRIVER'],
    'user' => $_ENV['DB_USER'],
    'password' => $_ENV['DB_PASSWORD'],
    'dbname' => $_ENV['DB_DATABASE'],
    'host' => $_ENV['DB_HOST']??'db',
);

// obtaining the entity manager
$entityManager = EntityManager::create($conn, $config);

\eduluz1976\monitor\Lib\GlobalContainer::set(ENTITY_MANAGER, $entityManager);

