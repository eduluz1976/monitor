<?php
namespace eduluz1976\monitor\Model;

//use Doctrine\ORM\Annotation as ORM;
use Doctrine\ORM\Mapping as ORM;
use eduluz1976\monitor\Lib\Identifiable;
use eduluz1976\monitor\Lib\OID;

/**
 * @ORM\Entity @ORM\Table(name="tasks")
 **/
class Task extends BaseModel implements Identifiable {

    use OID;

    const TYPE_STATE = 1;
    const TYPE_POINT = 2;

    const STATUS_INACTIVE=0;
    const STATUS_ACTIVE=1;

    /**
     * @var int
     * @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue
     */
    protected $id;

    /**
     * Means the type of return expected on this Task
     *
     * @var int
     * @ORM\Column(type="integer" )
     */
    protected $type=self::TYPE_STATE;

    /**
     * The current status of this task
     *
     * @var int
     * @ORM\Column(type="integer", nullable=true )
     */
    protected $status=self::STATUS_ACTIVE;

    /**
     * When was the last check? In seconds.
     * @var long
     * @ORM\Column(type="integer")
     */
    protected $last_check=0;

    /**
     * How many seconds take to re-run this task?
     * @var long
     * @ORM\Column(type="integer")
     */
    protected $interval_to_check=60;

    /**
     * Name of this task. Just informational.
     * @var string
     * @ORM\Column(type="string", nullable=true, length=80)
     */
    protected $name;

    /**
     * Code
     * @var string
     * @ORM\Column(type="text", nullable=true )
     */
    protected $code='';


    public function __construct()
    {
        parent::parent();
        $this->generateOID();
    }

}
