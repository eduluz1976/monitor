<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-05-08
 * Time: 10:35 PM
 */

namespace eduluz1976\monitor\Model;


class BaseModel
{

    public function bind($arr=[]) {

        $attrs = get_object_vars($this);

        foreach ($arr as $k=>$v) {
            if (array_key_exists($k, $attrs)) {
                $this->$k = $v;

            }
        }

    }

    public function __call($name, $arguments)
    {
        $arr = get_object_vars($this);
        if (isset($arr[$name])) {
            return $arr[$name];
        } else {
            return (isset($arguments[0])?$arguments[0]:null);
        }

    }

}
