<?php

namespace eduluz1976\monitor\Lib;

class GlobalContainer {

    protected static $data=[];


    public static function set($name, $value) {
        self::$data[$name] =  $value;
    }

    public static function get($name, $default=null) {
        if (isset(self::$data[$name])) {
            return self::$data[$name];
        } else {
            return $default;
        }
    }


}
