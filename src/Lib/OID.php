<?php
namespace eduluz1976\monitor\Lib;

trait  OID  {
    protected $_oid;

    public function getOID() {
        return $this->_oid;
    }

    public function generateOID() {
        $this->_oid = uniqid();
    }
}
