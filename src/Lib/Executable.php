<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-05-08
 * Time: 9:44 PM
 */

namespace eduluz1976\monitor\Lib;


interface Executable
{
    public function execute();
}
