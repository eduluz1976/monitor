<?php
/**
 * Created by PhpStorm.
 * User: eduardoluz
 * Date: 2019-05-08
 * Time: 9:42 PM
 */

namespace eduluz1976\monitor\Service;


use eduluz1976\monitor\Lib\Executable;
use eduluz1976\monitor\Model\Task;

class PerformTaskService implements Executable
{

    private $model;

    public function execute()
    {
        $code = $this->model->code();
        $return = eval($code);
        return $return;
    }

    public static function fromTask(Task $task) : PerformTaskService {
        $obj = new self();
        $obj->model=$task;
        return $obj;
    }

    public static function fromTaskID(int $taskID) : PerformTaskService {

        $entityManager = \eduluz1976\monitor\Lib\GlobalContainer::get(ENTITY_MANAGER);
        $task = $entityManager->find(Task::class, $taskID);

        $obj = new self();
        $obj->model=$task;
        return $obj;
    }

}
